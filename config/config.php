<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/4/14
 * Time: 6:18 PM
 */
return [
    'modes'   => [
        'xml',
        'javascript',
        'clike',
        'php',
    ],
    'options' => [
        'lineNumbers'    => true,
        'matchBrackets'  => true,
        'htmlMode'       => true,
        'mode'           => 'text/x-php',
        'indentUnit'     => 4,
        'indentWithTabs' => false,
        'enterMode'      => 'keep',
        'tabMode'        => 'shift',
    ],
];
