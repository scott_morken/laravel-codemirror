module.exports = function(grunt) {
    grunt.initConfig({
        copy: {
            target: {
                files: [
                    {
                        expand: true,
                        cwd: 'bower_components/',
                        src: ['codemirror/**'],
                        dest: 'public/assets/',
                        filter: 'isFile'
                    }
                ]
            }
        },
        uglify: {
            options: {
                mangle: false
            },
            codemirror: {
                files: {
                    './public/assets/codemirror/lib/codemirror.min.js': './public/assets/codemirror/lib/codemirror.js'
                }
            }
        }

    });
    // Plugin loading
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');


    // Task definition
    grunt.registerTask('default', ['copy', 'uglify']);
};
