<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/4/14
 * Time: 6:25 PM
 */

namespace Smorken\Codemirror\Facades;

use Illuminate\Support\Facades\Facade as F;

class Codemirror extends F
{

    protected static function getFacadeAccessor()
    {
        return 'smorken.codemirror';
    }
}
