<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/4/14
 * Time: 6:28 PM
 */

namespace Smorken\Codemirror;

use Illuminate\View\Factory;
use Smorken\Html\FormBuilder;

class Codemirror
{

    protected $config;
    protected $registered = [];

    /**
     * Illuminate view environment.
     *
     * @var \Illuminate\View\Factory
     */
    protected $view;

    public function __construct(Factory $view, FormBuilder $form, $config)
    {
        $this->view = $view;
        $this->form = $form;
        $this->config = $config;
    }

    public function create($name, $value = null, $options = [])
    {
        $id = $this->form->getIdAttribute($name, $options);
        if ($id === null) {
            $id = $this->generateId($name);
            $options['id'] = $id;
        }
        $txt = $this->form->textarea($name, $value, $options);
        $this->register($id);
        return $txt;
    }

    public function generateId($name)
    {
        $count = count($this->registered());
        return $name . '-' . $count;
    }

    public function registered()
    {
        return $this->registered;
    }

    public function register($id)
    {
        $this->registered[] = $id;
    }

    /**
     * @param $request
     * @param \Symfony\Component\HttpFoundation\Response $response
     * Borrowed from Barryvdh/LaravelDebugBar
     */
    public function modifyResponse($request, $response)
    {
        $js = $this->generateJavascript();
        if ($js) {
            $content = $response->getContent();

            $pos = mb_strripos($content, '</body>');
            if (false !== $pos) {
                $content = mb_substr($content, 0, $pos) . $js . mb_substr($content, $pos);
            } else {
                $content = $content . $js;
            }
            $response->setContent($content);
        }
    }

    public function generateJavascript()
    {
        if ($this->isRegistered()) {
            return $this->view->make(
                'smorken/codemirror::codemirror',
                [
                    'modes'      => $this->modes(),
                    'registered' => $this->registered(),
                ]
            );
        }
    }

    public function isRegistered()
    {
        return count($this->registered) > 0;
    }

    public function modes()
    {
        return array_get($this->config, 'modes', []);
    }
}
