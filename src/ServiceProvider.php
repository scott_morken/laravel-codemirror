<?php namespace Smorken\Codemirror;

use Illuminate\Support\ServiceProvider as SP;

class ServiceProvider extends SP
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../views', 'smorken/codemirror');
        $this->publishes(
            [
                __DIR__ . '/../views' => base_path() . '/resources/views/vendor/smorken/codemirror',
            ],
            'views'
        );
        $this->publishes(
            [
                __DIR__ . '/../config' => config_path() . '/vendor/smorken/codemirror',
            ],
            'config'
        );
        $this->publishes(
            [
                __DIR__ . '/../public/assets/codemirror' => base_path() . '/public/js/codemirror',
            ],
            'public'
        );

        $cm = $this->app['smorken.codemirror'];
        $app = $this->app;
        $app['router']->after(
            function ($request, $response) use ($cm) {
                $cm->modifyResponse($request, $response);
            }
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerResources();
        $this->app['smorken.codemirror'] = $this->app->share(
            function ($app) {
                return new Codemirror($app['view'], $app['form'], config('smorken/codemirror::config', []));
            }
        );
    }

    protected function registerResources()
    {
        $files = ['config'];
        foreach ($files as $file) {
            $userconfigfile = sprintf("%s/vendor/smorken/codemirror/%s.php", config_path(), $file);
            $packageconfigfile = sprintf("%s/../config/%s.php", __DIR__, $file);
            $this->registerConfig($packageconfigfile, $userconfigfile, 'smorken/codemirror::' . $file);
        }
    }

    protected function registerConfig($packagefile, $userfile, $namespace)
    {
        $config = $this->app['files']->getRequire($packagefile);
        if (file_exists($userfile)) {
            $userconfig = $this->app['files']->getRequire($userfile);
            $config = array_replace_recursive($config, $userconfig);
        }
        $this->app['config']->set($namespace, $config);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['smorken.codemirror'];
    }
}
