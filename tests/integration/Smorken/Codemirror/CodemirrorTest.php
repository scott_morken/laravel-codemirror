<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/6/14
 * Time: 12:34 PM
 */

use Mockery as m;

class CodemirrorIntegrationTest extends AppTestCase
{

    /**
     * @var \Smorken\Codemirror\Codemirror
     */
    protected $sut;

    public function setUp()
    {
        parent::setUp();
        $this->sut = app('smorken.codemirror');
    }

    public function testCreateSetsIsRegistered()
    {
        $txt = $this->sut->create('foo', 'bar', []);
        $expected = '<textarea id="foo-0" name="foo" cols="50" rows="10">bar</textarea>';
        $this->assertEquals($expected, $txt);
        $this->assertTrue($this->sut->isRegistered());
    }

    public function testGenerateJavascript()
    {
        $txt = $this->sut->create('foo', 'bar', []);
        $out = $this->sut->generateJavascript();
        $this->assertTrue(str_contains($out, 'var codemirroreditfoo0'));
    }

    protected function getConfig()
    {
        return [
            'modes'   => [
                'xml',
                'javascript',
                'clike',
                'php',
            ],
            'options' => [
                'lineNumbers'    => true,
                'matchBrackets'  => true,
                'mode'           => 'text/x-php',
                'indentUnit'     => 4,
                'indentWithTabs' => false,
                'enterMode'      => 'keep',
                'tabMode'        => 'shift',
            ],
        ];
    }
}
