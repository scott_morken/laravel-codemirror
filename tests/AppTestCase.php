<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/7/14
 * Time: 8:53 AM
 */

use Mockery as m;

abstract class AppTestCase extends Orchestra\Testbench\TestCase
{

    protected function getPackageProviders($app)
    {
        return [
            \Smorken\Html\HtmlServiceProvider::class,
            \Smorken\Codemirror\ServiceProvider::class,
        ];
    }
}
