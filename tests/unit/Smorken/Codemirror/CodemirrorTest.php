<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/6/14
 * Time: 12:34 PM
 */

use Illuminate\View\Environment;
use Mockery as m;
use Smorken\Codemirror\Codemirror;

class CodemirrorTest extends PackageTestCase
{

    /**
     * @var \Smorken\Codemirror\Codemirror
     */
    protected $sut;

    public function setUp()
    {
        $this->view = m::mock('\Illuminate\View\Factory');
        $this->form = m::mock('\Smorken\Html\FormBuilder');
        $this->sut = new Codemirror($this->view, $this->form, $this->getConfig());
    }

    public function testDefaultIsNotRegistered()
    {
        $this->assertFalse($this->sut->isRegistered());
    }

    public function testRegisterSetsIsRegistered()
    {
        $this->sut->register(1);
        $this->assertTrue($this->sut->isRegistered());
    }

    public function testCreateSetsIsRegistered()
    {
        $this->form->shouldReceive('textarea')
                   ->once()
                   ->with('foo', 'bar', [])
                   ->andReturn('textarea html');
        $this->form->shouldReceive('getIdAttribute')
                   ->once()
                   ->with('foo', [])
                   ->andReturn('foo-id');
        $txt = $this->sut->create('foo', 'bar', []);
        $this->assertEquals('textarea html', $txt);
        $this->assertTrue($this->sut->isRegistered());
    }

    public function testGenerateJavascriptWithNoRegistersReturnsNull()
    {
        $this->assertNull($this->sut->generateJavascript());
    }

    public function testGenerateJavascript()
    {
        $this->view->shouldReceive('make')
                   ->once()
                   ->with(
                       'smorken/codemirror::codemirror',
                       ['modes' => $this->getConfig()['modes'], 'registered' => ['id-1']]
                   )
                   ->andReturn('generated javascript');
        $this->sut->register('id-1');
        $out = $this->sut->generateJavascript();
        $this->assertEquals('generated javascript', $out);
    }

    protected function getConfig()
    {
        return [
            'modes'   => [
                'xml',
                'javascript',
                'clike',
                'php',
            ],
            'options' => [
                'lineNumbers'    => true,
                'matchBrackets'  => true,
                'mode'           => 'text/x-php',
                'indentUnit'     => 4,
                'indentWithTabs' => false,
                'enterMode'      => 'keep',
                'tabMode'        => 'shift',
            ],
        ];
    }
}
