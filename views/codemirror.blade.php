<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/4/14
 * Time: 6:14 PM
 */
/* @var array $modes
 * @var array $registered
 */
?>
<link rel="stylesheet" type="text/css" href="{{ asset('js/codemirror/lib/codemirror.css') }}"/>
@if (app()->environment('local', 'development'))
    <script type="text/javascript" src="{{ asset('js/codemirror/lib/codemirror.js') }}"></script>
@else
    <script type="text/javascript" src="{{ asset('js/codemirror/lib/codemirror.min.js') }}"></script>
@endif
@foreach($modes as $mode)
    <script type="text/javascript" src="{{ asset('js/codemirror/mode/' . $mode . '/' . $mode . '.js') }}"></script>
@endforeach

@foreach($registered as $id)
    @include('smorken/codemirror::jsline', array('id' => $id))
@endforeach
