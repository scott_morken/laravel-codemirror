<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/4/14
 * Time: 6:43 PM
 */
$options = json_encode(config('smorken/codemirror::config.options'));
?>
<script type="text/javascript" id="codemirror-{{ $id }}">
    var codemirroredit{{ camel_case($id) }} = CodeMirror.fromTextArea(document.getElementById('{{ $id }}'), {!! $options !!});
</script>
